package com.evgeny.bozhko.cities_quiz.data;

/**
 * Created by smail2133 on 3/8/18.
 */

public interface ContentLoader {

    void requestContent(OnContentLoaded contentLoaded);
}
