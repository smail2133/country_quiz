package com.evgeny.bozhko.cities_quiz.deps;

import com.evgeny.bozhko.cities_quiz.data.AssetsContentLoader;
import com.evgeny.bozhko.cities_quiz.ui.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by smail2133 on 3/8/18.
 */

@Component(modules = AssetsContentLoader.class)
@Singleton
public interface AppComponent {

    void inject(MainActivity activity);
}
