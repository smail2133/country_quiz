package com.evgeny.bozhko.cities_quiz.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.evgeny.bozhko.cities_quiz.R;
import com.kaopiz.kprogresshud.KProgressHUD;

import butterknife.ButterKnife;

/**
 * Created by smail2133 on 3/8/18.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private KProgressHUD progressHUD;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentResource());
        ButterKnife.bind(this);
    }

    protected abstract int getContentResource();

    protected void showError(String error) {
        Toast.makeText(this, String.valueOf(error), Toast.LENGTH_LONG).show();
    }

    protected void logE(String error, Exception e) {
        Log.e(getClass().getCanonicalName(), String.valueOf(error), e);
    }

    public void showSpinner(String details) {
        if (progressHUD == null) {
            progressHUD = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel(getString(R.string.please_wait))
                    .setDetailsLabel(details)
                    .setCancellable(true)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show();
        } else {
            progressHUD.setDetailsLabel(details);
        }
    }

    public void hideSpinner() {
        if (progressHUD != null) {
            progressHUD.dismiss();
        }
    }
}
