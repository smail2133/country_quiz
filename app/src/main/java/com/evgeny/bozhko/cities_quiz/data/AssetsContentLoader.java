package com.evgeny.bozhko.cities_quiz.data;

import android.util.Log;

import com.evgeny.bozhko.cities_quiz.R;
import com.evgeny.bozhko.cities_quiz.data.entity.JsonCities;
import com.evgeny.bozhko.cities_quiz.ui.BaseActivity;
import com.evgeny.bozhko.cities_quiz.ui.MainActivity;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

/**
 * Created by smail2133 on 3/8/18.
 */

@Module
public class AssetsContentLoader implements ContentLoader {

    private static final String TAG = AssetsContentLoader.class.getCanonicalName();

    private BaseActivity activity;

    @Inject
    public AssetsContentLoader(MainActivity activity) {
        this.activity = activity;
    }

    @Provides
    AssetsContentLoader provideSelf() {
        return this;
    }

    @Override
    public void requestContent(final OnContentLoaded listener) {
        activity.showSpinner(activity.getString(R.string.downloading_data));

        new Thread(new Runnable() {
            @Override
            public void run() {
                Scanner scanner = null;
                InputStream jsF = null;

                try {
                    jsF = activity.getAssets().open("capitalCities.json");

                    BufferedReader reader = new BufferedReader(new InputStreamReader(jsF));
                    final StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    reader.close();

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JsonCities model = new Gson().fromJson(sb.toString(), JsonCities.class);
                                listener.onContentLoaded(model);
                            } catch (JsonParseException exception) {
                                listener.onError(exception);
                            }

                            activity.hideSpinner();
                        }
                    });
                } catch (IOException e) {
                    Log.e(TAG, "error open stream", e);
                    listener.onError(e);
                } finally {
                    if (scanner != null) {
                        scanner.close();
                    }
                    if (jsF != null) {
                        try {
                            jsF.close();
                        } catch (IOException e) {
                            Log.e(TAG, "error close stream", e);
                        }
                    }
                }
            }
        }).start();
    }
}
