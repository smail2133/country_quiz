package com.evgeny.bozhko.cities_quiz.data;

import com.evgeny.bozhko.cities_quiz.data.entity.BaseModel;

/**
 * Created by smail2133 on 3/8/18.
 */

public interface OnContentLoaded {

    void onContentLoaded(BaseModel content);

    void onError(Exception e);
}
