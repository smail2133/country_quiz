package com.evgeny.bozhko.cities_quiz.ui;

import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.evgeny.bozhko.cities_quiz.R;
import com.evgeny.bozhko.cities_quiz.data.AssetsContentLoader;
import com.evgeny.bozhko.cities_quiz.data.OnContentLoaded;
import com.evgeny.bozhko.cities_quiz.data.entity.BaseModel;
import com.evgeny.bozhko.cities_quiz.data.entity.JsonCities;
import com.evgeny.bozhko.cities_quiz.deps.DaggerAppComponent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;

public class MainActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener,
        GoogleMap.OnMapLoadedCallback, GoogleMap.OnMapClickListener, OnContentLoaded {

    private static LatLngBounds EUROPE = new LatLngBounds(
            new LatLng(36.242278, -12.449872), new LatLng(70.355219, 31.356680));
    private static final int MAX_ZOOM = 5;
    private static final int MIN_ZOOM = 4;
    private static final int KM_HEALTH = 1500;
    private static final int MISTAKE_RADIUS_KM = 50;

    @BindView(R.id.cities_value)
    TextView citiesValue;
    @BindView(R.id.kilometer_value)
    TextView kilometersValue;
    @BindView(R.id.task_value)
    TextView taskValue;
    @BindView(R.id.place_button)
    Button placeButton;
    @BindView(R.id.repeat_button)
    Button repeatButton;

    private GoogleMap map;
    private Geocoder geocoder;
    private JsonCities.City currentCity;
    private Marker markerSelected;

    private List<JsonCities.City> cities;

    private int kilometersLeft = KM_HEALTH;
    private int totalCitiesCount;

    @Inject
    AssetsContentLoader assetsContentLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerAppComponent.builder()
                .assetsContentLoader(new AssetsContentLoader(this))
                .build().inject(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        placeButton.setOnClickListener(this);
        repeatButton.setOnClickListener(this);

        assetsContentLoader.requestContent(this);

        geocoder = new Geocoder(this);
    }

    @Override
    protected int getContentResource() {
        return R.layout.activity_main;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMaxZoomPreference(MAX_ZOOM);
        map.setMinZoomPreference(MIN_ZOOM);
        map.setOnMapLoadedCallback(this);
        map.setOnMapClickListener(this);

        try {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style));
        } catch (Resources.NotFoundException e) {
            logE("Error load style", e);
        }

        UiSettings mapUI = map.getUiSettings();

        mapUI.setCompassEnabled(false);
        mapUI.setZoomControlsEnabled(true);
        mapUI.setMapToolbarEnabled(false);
    }

    @Override
    public void onClick(View v) {
        if (repeatButton == v) {
            onRepeat();
        } else if (placeButton == v) {
            checkPlace();
        }
    }

    @Override
    public void onMapLoaded() {
        map.setLatLngBoundsForCameraTarget(EUROPE);
        map.animateCamera(CameraUpdateFactory.newLatLng(EUROPE.getCenter()));
    }

    @Override
    public void onMapClick(LatLng latLng) {
        map.clear();
        markerSelected = map.addMarker(new MarkerOptions().position(latLng));
    }

    @Override
    public void onContentLoaded(BaseModel content) {
        if (content instanceof JsonCities) {
            cities = ((JsonCities) content).getCapitalCities();
            totalCitiesCount = cities.size();

            nextCity();
        }
    }

    @Override
    public void onError(Exception e) {
        logE("content error load", e);
        showError("content load error \n" + e.getCause());

        placeButton.setEnabled(false);
    }

    private void initUiWithModel(JsonCities.City city) {
        citiesValue.setText(String.format(getString(R.string.cities_placed), String.valueOf(totalCitiesCount - cities.size())));
        kilometersValue.setText(String.format(getString(R.string.kilometers_left), String.valueOf(kilometersLeft)));
        taskValue.setText(String.format(getString(R.string.select_the_location), String.valueOf("\"" + city.capitalCity + "\"")));
    }

    private void nextCity() {
        if (cities.size() > 0) {
            markerSelected = null;
            map.clear();

            int rand = new Random().nextInt(cities.size() - 1);
            currentCity = cities.get(rand);

            Location cityLocation = extractLocation(currentCity.capitalCity);

            if (cityLocation != null) {
                currentCity.cityLocation = cityLocation;
                initUiWithModel(currentCity);
                placeButton.setEnabled(true);
                cities.remove(currentCity);
            } else {
                cities.remove(currentCity);
                nextCity();
            }
        } else {
            repeatButton.setVisibility(View.VISIBLE);
        }
    }

    private void checkPlace() {
        if (markerSelected != null) {
            Location markerLocation = new Location("");
            markerLocation.setLongitude(markerSelected.getPosition().longitude / 1e6);
            markerLocation.setLatitude(markerSelected.getPosition().latitude / 1e6);

            float kmToCorrect = (currentCity.cityLocation.distanceTo(markerLocation) * 1000);

            if (kmToCorrect <= MISTAKE_RADIUS_KM) {
                nextCity();
            } else {
                kilometersLeft -= kmToCorrect;

                if (kilometersLeft <= 0) {
                    Toast.makeText(this, getString(R.string.you_lose), Toast.LENGTH_SHORT).show();
                    repeatButton.setVisibility(View.VISIBLE);
                    placeButton.setEnabled(false);
                    initUiWithModel(currentCity);
                } else {
                    nextCity();
                }
            }
        } else {
            Toast.makeText(this, getString(R.string.please_select_place), Toast.LENGTH_LONG).show();
        }
    }

    private Location extractLocation(String address) {
        try {
            List<Address> locations = geocoder.getFromLocationName(address, 1);

            if (locations.size() > 0) {
                Address temp = locations.get(0);
                Location location = new Location("");
                location.setLatitude(temp.getLatitude() / 1e6);
                location.setLongitude(temp.getLongitude() / 1e6);

                return location;
            } else {
                return null;
            }
        } catch (IOException e) {
            logE("error extract location", e);

            return null;
        }
    }

    private void onRepeat() {
        assetsContentLoader.requestContent(this);
        map.clear();
        markerSelected = null;
        kilometersLeft = KM_HEALTH;
        repeatButton.setVisibility(View.GONE);
    }
}
