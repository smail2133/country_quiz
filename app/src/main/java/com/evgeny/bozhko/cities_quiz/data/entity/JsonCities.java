package com.evgeny.bozhko.cities_quiz.data.entity;

import android.location.Location;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by smail2133 on 3/8/18.
 */

public class JsonCities extends BaseModel {

    @SerializedName("capitalCities")
    private List<City> capitalCities;

    public List<City> getCapitalCities() {
        return capitalCities;
    }

    public void setCapitalCities(List<City> capitalCities) {
        this.capitalCities = capitalCities;
    }

    public static class City extends BaseModel {

        @SerializedName("capitalCity")
        public String capitalCity;
        @SerializedName("lat")
        public String latitude;
        @SerializedName("long")
        public String longitude;

        public Location cityLocation;
    }
}
